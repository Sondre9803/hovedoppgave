// Hide the second image initially
document.getElementById("image2").style.display = "none";

function showImage(imageId) {
  if (imageId === "image1") {
    document.getElementById("image1").style.display = "block";
    document.getElementById("image2").style.display = "none";
  } else if (imageId === "image2") {
    document.getElementById("image1").style.display = "none";
    document.getElementById("image2").style.display = "block";
  }
}
