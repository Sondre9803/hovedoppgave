const playButtons = document.querySelectorAll(".play-button");

playButtons.forEach((button, index) => {
  button.addEventListener("click", () => {
    const audioElement = button
      .closest(".audio-player")
      .querySelector(".audio-element");
    const playButton = button
      .closest(".audio-player")
      .querySelector(".play-button");

    if (audioElement.paused) {
      audioElement.play();
      playButton.classList.add("playing");
    } else {
      audioElement.pause();
      playButton.classList.remove("playing");
    }
  });

  const audioElement = button
    .closest(".audio-player")
    .querySelector(".audio-element");
  audioElement.addEventListener("error", function () {
    console.log("Error: Could not play audio file.");
  });
});

function playMusic() {
  console.log("Playing music...");
}

const playButton = document.getElementById("play-button");
playButton.addEventListener("click", playMusic);

function play() {
  var audio = document.getElementById("audio");
  audio.play();
}

function prev() {
  console.log("previous");
}

function next() {
  console.log("next");
}

function randoms() {
  console.log("shuffle");
}

function repo() {
  console.log("repeat");
}
// toggle av og på play
function togglePlay() {
  var audio = document.getElementById("audio");
  var playIcon = document.getElementById("play-icon");
  var progress = document.querySelector(".progress");
  var thumb = document.querySelector(".thumb");
  if (audio.paused) {
    audio.play();
    playIcon.classList.remove("fa-play");
    playIcon.classList.add("fa-pause");
    findTotalAudioTime(); // Start updating time
    updateProgress(audio, progress, thumb); // Start updating progress bar
  } else {
    audio.pause();
    playIcon.classList.remove("fa-pause");
    playIcon.classList.add("fa-play");
    clearInterval(timerInterval); // Stop the timer
    clearInterval(progressInterval); // Stop the progress bar update
    timerInterval = null;
    progressInterval = null;

    currentMinutes = Math.floor(audio.currentTime / 60);
    currentSeconds = Math.floor(audio.currentTime % 60);
  }
}

function findTotalAudioTime() {
  let getaudio = document.getElementById("audio").duration;
  let minutes = Math.floor(getaudio / 60);
  let seconds = Math.floor(getaudio % 60);

  // Pad single-digit seconds with leading zero
  seconds = seconds < 10 ? "0" + seconds : seconds;

  let totalTimeString = minutes + ":" + seconds;

  let getTotalAudioTime = document.getElementById("totalplaytime");
  getTotalAudioTime.innerHTML = totalTimeString;

  trackPlayTime();
}

function trackPlayTime() {
  let getaudio = document.getElementById("audio").duration;
  let minutes = Math.floor(getaudio / 60);
  let seconds = Math.floor(getaudio % 60);
  let currentMinutes = 0;
  let currentSeconds = 0;
  let timerInterval = setInterval(function () {
    // Pad single-digit current seconds with leading zero
    let currentSecondsFormatted =
      currentSeconds < 10 ? "0" + currentSeconds : currentSeconds;

    let currentTimeString = currentMinutes + ":" + currentSecondsFormatted;
    let getCurrentAudioTime = document.querySelector(".current-time");
    getCurrentAudioTime.innerHTML = currentTimeString;

    if (currentMinutes === minutes && currentSeconds === seconds) {
      clearInterval(timerInterval); // Stop the timer
    } else {
      // Increment the current time by 1 second
      if (currentSeconds === 59) {
        currentMinutes++;
        currentSeconds = 0;
      } else {
        currentSeconds++;
      }
    }
  }, 1000);
}

/* Music bar at the end of the page

  1. Lag noe Visuelt først.
    - Bar nederst på siden.
    - Bakgrunn farge
    - Iconer
    - Bilde på sang
    - Progress bar til sang
    - ...

  2. Gjør hver del i seekbaren klikkbar.
    - Gi en alert / console.log som viser at man har klikket på dette.

  3. Funksjon 1. PlaySongER -   Skal spille en sang.
    - Trykk play - Spill en sang
    - Kan være hardkodet.

  4. Funksjon 2. - Shuffler -  Spill en tilfeldig sang
    - Skal kunne velge en RANDOM sang, ved å klikke på iconet (randomiser)
    - Skal vise i console.log at denne sangen er valgt.
    - Skal vises på skjermen, at denne sangen er valgt.
    - Skal kunne sangen når man trykker på funksjon 1.

  5. Funksjon 3. Vis frem informasjon om denne sangen.
    - Skal kunne vise bilde, tekst, navn, beskrivelse .... om sangen.
    - Vis denne dataen i console.log
    - Vis denne dataen på siden.
    
  6. Funksjon 4 - Hopp til neste sang.
    - Velg neste object i sang arrayet.
    - Vis dette på skjermen.

  7. Funksjon 5 - Hopp til forige sang.

  8. Funksjon 6 - Repeat sang

  9. Funksjon 7 - Volum

  10. Basert på tid, vis dette i en progressbar.
    - Vis nedtelling av sang / sang tid i sekunder (opptelling)
    - 

  11. Kunne pause en sang

  12. .......

  13. Justere det visuelle.
    - Ha hover på knappene
    - Tooltip
    - Mer.....


  14. Hent sanger fra en API
*/
