// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function () {
  modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

function sendForm() {
  alert("Message Sent will reply in 1-2 hours");
}

function cancelForm() {
  // Perform any desired action, such as redirecting to another page:
  window.location.href = "";
}

function toggleModal(modalStatus) {
  let modalen = document.getElementById("myModal");

  if (modalStatus == true) {
    document.getElementById("myModal").classList.remove("modalHidden");
    // modalen.classList.add("modalDisplayed");
    console.log(modalStatus);
  } else {
    document.getElementById("myModal").classList.add("modalHidden");
    console.log(modalStatus);
  }
}
